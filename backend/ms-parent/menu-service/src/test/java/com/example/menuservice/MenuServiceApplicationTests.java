package com.example.menuservice;

import com.example.menuservice.menu.CreateMenu;
import com.example.menuservice.menu.MenuRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@Testcontainers
@AutoConfigureMockMvc
class MenuServiceApplicationTests {

	@Container
	static MongoDBContainer container = new MongoDBContainer("mongo:latest");
	@Autowired
	private MockMvc mockMvc;
	@Autowired
	private ObjectMapper mapper;
	@Autowired
	private MenuRepository repository;

	@DynamicPropertySource
	static void setProperties(DynamicPropertyRegistry registry) {
		registry.add("spring.data.mongodb.uri", container::getReplicaSetUrl);
	}

	@Test
	void shouldCreateMenu() throws Exception {
		CreateMenu menu = getMenuRequest();
		String createMenuJson = mapper.writeValueAsString(menu);

		mockMvc.perform(MockMvcRequestBuilders
			.post("/api/menu/")
				.contentType(MediaType.APPLICATION_JSON)
				.content(createMenuJson))
				.andExpect(status().isOk());
		Assertions.assertEquals(1, repository.findAll().size());
	}

	private CreateMenu getMenuRequest() {
		return CreateMenu.builder()
				.name("Pizza")
				.price(20)
				.build();
	}
}
