package com.example.menuservice.menu;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.bson.types.Binary;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Document(value = "menus")
public class Menu {
    @Id
    @NotNull
    private String id;
    @NotNull
    private String name;
    @NotNull
    private int price;
    private Binary image;
}
