package com.example.menuservice.utils;

import lombok.experimental.UtilityClass;

@UtilityClass
public class ValidationMessages {
	public static final String USERNAME_ERR_MSG = "Username is not valid! Length must be in 2-32 characters," +
			" letters, numbers and special characters are allowed! (allowed special characters: @#$%^&+=)";

	public static final String PSWD_ERR_MSG = "Password is not valid! " +
			"Password must contain at least 2 digits, " +
			"and minimum one lowercase alphabet, " +
			"and minimum one uppercase alphabet," +
			"and minimum one of the following special characters: @#$%^&+= " +
			"while spaces are not allowed, " +
			"and password length is minimum 8 but maximum 20 characters long.";
}