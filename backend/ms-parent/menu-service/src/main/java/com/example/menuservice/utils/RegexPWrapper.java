package com.example.menuservice.utils;

import lombok.experimental.UtilityClass;

@UtilityClass
public class RegexPWrapper {
	public static final String ROLE = "^(admin|supervisor|customer)$";
	public static final String USERNAME = "^[0-9a-zA-Z@#$%^&+=_]{2,64}$";
	public static final String F_AND_L_NAME = "^[a-zA-Z ]{2,64}$";
	public static final String EMAIL = "^[a-z0-9._-]{3,80}@[a-z0-9._-]{2,45}\\.[a-z]{2,3}$";
	public static final String PASSWORD = "^(?=.*[0-9]{2})"
																								+ "(?=.*[a-z])(?=.*[A-Z])"
																								+ "(?=.*[@#$%^&+=])"
																								+ "(?=\\S+$).{8,64}$";
}