package com.example.menuservice.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Document(value = "users")
public class User {
    @Id
    private String id;
    private String firstName;
    private String lastName;
    private int age;
    private String email;
    private String about;
}
