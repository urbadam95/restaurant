package com.example.menuservice.menu;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.NoSuchElementException;

@Service
@Transactional
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class MenuService {
    private final MenuRepository repository;

    public MenuResponse getById(String id) {
        Menu menu = repository.findById(id).orElseThrow(() -> {
            throw new NoSuchElementException("No menu was present by this id: " + id);
        });

        return mapToMenuResponse(menu);
    }

    public MenuResponse create(CreateMenu dto) {
        Menu menu = Menu.builder()
                .name(dto.getName())
                .price(dto.getPrice())
                .image(dto.getImage())
                .build();

        return mapToMenuResponse(repository.save(menu));
    }

    public MenuResponse update(UpdateMenu dto) {
        Menu menu = repository.findById(dto.getId()).orElseThrow(() -> {
            throw new NoSuchElementException("No menu was present by this id: " + dto.getId());
        });

        menu.setName(dto.getName());
        menu.setPrice(dto.getPrice());
        menu.setImage(dto.getImage());

        return mapToMenuResponse(repository.save(menu));
    }

    public List<MenuResponse> getAll() {
        List<Menu> menus = repository.findAll();

        return menus.stream().map(this::mapToMenuResponse).toList();
    }

    public void delete(String id) {
        repository.deleteById(id);
    }

    private MenuResponse mapToMenuResponse(Menu menu) {
        return MenuResponse.builder()
                .id(menu.getId())
                .name(menu.getName())
                .price(menu.getPrice())
                .image(menu.getImage())
                .build();
    }
}
